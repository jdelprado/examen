import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith (value = Parameterized.class)
public class ProvesParametritzada {


	private double delta,actual,esperado;
	private Primordial numPrimer;

	@Parameters
	public static Collection<Double[]> testParam(){
		return Arrays.asList(new Double[][]{
			{2.0, 2.0, 0.03},
			{6.0, 3.0, 0.03},
			{6.0, 4.0, 0.03},
			{30.0, 5.0, 0.03},
			{210.0, 7.0, 0.03},
			{2310.0, 11.0, 0.03},
			{30030.0, 13.0, 0.03},
			{510510.0, 17.0, 0.03},
			{9699690.0, 19.0, 0.03},
			{223092870.0, 23.0, 0.03},

		});
	}
	public ProvesParametritzada(double esperado,double actual,double delta) {
		this.esperado = esperado;
		this.actual = actual;
		this.delta = delta;
	}

	@Before
	public void crearObjecte(){
		numPrimer = new Primordial();
	}


	@Test
	public void test() {
		double calculado = numPrimer.get_primordial(actual);
		Assert.assertEquals(esperado, calculado,delta);
	}


	@Test (expected = IllegalArgumentException.class)
	public void test2(){

		System.out.println(numPrimer.get_primordial(-5));

	}

	@Test
	public void test3(){
		//Evidentemente falla porque son objetos diferentes
		Assert.assertTrue(numPrimer.get_primordial(8) > 200);
		Assert.assertFalse(numPrimer.get_primordial(6) > 30);
		Assert.assertTrue(numPrimer.get_primordial(2) <= 2);

	}

}
